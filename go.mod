module gitlab.com/mainmast/microservices/iam/migrations.git

require (
	github.com/golang-migrate/migrate/v4 v4.7.0
	github.com/lib/pq v1.2.0
)

go 1.13
